const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const ejs = require("ejs")
const path = require('path');
const saltedMd5 = require('salted-md5');
const cookieParser = require('cookie-parser')
const routes = require('./routes/routes')
require('dotenv').config()

const app = express()

//db
// mongoose
// .connect(process.env.db,
//     {useNewUrlParser: true,})
// .then(() => console.log('Connected to DB'))



//database
mongoose.connect("mongodb://localhost:27017/capitallDB", { useNewUrlParser: true, useUnifiedTopology: true });

//Customer-ID
const customerSchema =({
  username: String,
  password: String
});

const Customer = new mongoose.model("Customer", customerSchema);

//USER
const campaignstatusSchema = ({
  closeddrive: String,
  opendrive: String
});
const Status = new mongoose.model("Status", campaignstatusSchema);

const portfolioSchema = ({
  campaignstatus : [campaignstatusSchema]
});
const Portfolio = new mongoose.model("Portfolio", portfolioSchema);

const featureSchema = ({
  paymentdetail: String,
  kycdetail: String,
  status: String
});
const Feature = new mongoose.model("Feature", featureSchema);

const nameSchema = ({
  email: String,
  address: String,
  age: String,
  portfolio: [portfolioSchema],
  feature: [featureSchema]
});
const Name = new mongoose.model("Name", nameSchema);

const userSchema =({
  name: [nameSchema]
});
const User = new mongoose.model("User", userSchema);


//CAMPAIGN
const campaignSchema =({
  goal: Number,
  totalamountraise: String,
  tenure: String,
  ticketsize: String,
  interestofreturn: String,
  riskanalysis: String
});
const Campaign1 = new mongoose.model("Campaign1", campaignSchema);
const Campaign2 = new mongoose.model("Campaign2", campaignSchema);

//COMPANY
const companySchema =({
  activecampaigns: String,
  failedcampaigns: String,
  successfullcampaigns: [campaignSchema]
});
const Company = new mongoose.model("Company", companySchema);




//middlewares
app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(cookieParser())
app.use('/api', routes)
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));




app.get("/", function(req, res){
  res.send("hello");
});

//get request for admin page
var numberofcampaigns = 2;
app.get("/admin", function(req, res){
  res.render("admin", {numberofcampaigns: numberofcampaigns});
});

//POST request from admin page
app.post("/admin", function(req, res){
  const campaignname = String(req.body.campaignname);
  numberofcampaigns = numberofcampaigns+1;

  req.body.campaignname = new mongoose.model(String(req.body.campaignname), campaignSchema);

});

//view all campaigns
app.get("/campaigns", function(req, res){
  res.render("campaigns", {numberofcampaigns: numberofcampaigns});
});



//login
app.get("/login", function(req, res){
  res.render("login")
});
app.post("/login", function(req, res){
  const username = req.body.username;
  const password = saltedMd5(req.body.password, 'SUPER-S@LT!');

  Customer.findOne({username: username}, function(err, founduser){
    if(!err){
      if(founduser){
        if(password == founduser.password){
          res.redirect("/campaigns");
        }
        else{
          res.send("incorrect Password");
        }
      }
      else{
        res.send("incorrect username");
      }

    }

  });


});
//Register
app.get("/register", function(req, res){
  res.render("register")
});
app.post("/register", function(req, res){
  const username = req.body.username;
  const password = saltedMd5(req.body.password, 'SUPER-S@LT!');

  Customer.findOne({username: username}, function(err, foundlist){
    if(!err){
      if(foundlist){
        res.send("username exist");

      }
      else{
        const newuser = new Customer ({
          username: username,
          password: password
        });
        newuser.save();
        res.send("user register successfully");
      }
    }
  });


});











//cors
if (process.env.NODE_ENV === 'development') {
    app.use(cors({origin: `${process.env.CLIENT_URL}`}))
}

//port config
const port = process.env.PORT || 3000
app.listen(port, () => {
    console.log(`Server is running on port ${port} `)
})
